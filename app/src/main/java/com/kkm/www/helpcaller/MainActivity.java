package com.kkm.www.helpcaller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    public void adminLogin(View view)
    {
        Intent intent=new Intent(getApplicationContext(),admin.class);
        startActivity(intent);

    }

    public void userLogin(View view)
    {
        Intent intent=new Intent(getApplicationContext(),user.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
