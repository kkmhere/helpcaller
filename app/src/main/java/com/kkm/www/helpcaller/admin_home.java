package com.kkm.www.helpcaller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class admin_home extends AppCompatActivity {
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        mAuth = FirebaseAuth.getInstance();
        Button logOut=(Button)findViewById(R.id.logOut);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        Button getData=(Button)findViewById(R.id.getData);
        final TextView finaltext1=(TextView)findViewById(R.id.clothText);
        final TextView finaltext2=(TextView)findViewById(R.id.foodText);
        final TextView finaltext3=(TextView)findViewById(R.id.locationText);
        final TextView userName=(TextView)findViewById(R.id.userName);





        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                startActivity(new Intent(admin_home.this, admin.class));
            }
        });

        getData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.getReference("vivek")
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String user=dataSnapshot.getRef().toString();
                                Toast.makeText(admin_home.this,user,Toast.LENGTH_SHORT).show();
                                userName.setText("user:"+user);
                                String name=dataSnapshot.child("CLOTH_REQUIRED").getValue(String.class);
                                finaltext1.setText("CLOTHES REQUIRED:"+name);
                                String name2=dataSnapshot.child("FOOD_REQUIRED").getValue(String.class);
                                finaltext2.setText("FOOD ITEMS REQUIRED:"+name2);
                                String name3=dataSnapshot.child("LOCATION").getValue(String.class);
                                finaltext3.setText("LOCATION:"+name3);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }
        });

    }
}
