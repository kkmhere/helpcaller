package com.kkm.www.helpcaller;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.Lifecycle;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class user extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        final EditText name=(EditText)findViewById(R.id.userName);
        final EditText location=(EditText)findViewById(R.id.locationText);
        final EditText food=(EditText)findViewById(R.id.foodText);
        final EditText clothing=(EditText)findViewById(R.id.clothText);
        Button sendRequest=(Button)findViewById(R.id.helpButton);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReferenceFromUrl("https://helpcaller-dbde6.firebaseio.com/");

        sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myRef.child(name.getText().toString()).child("LOCATION")
                        .setValue(location.getText().toString());
                myRef.child(name.getText().toString()).child("FOOD_REQUIRED")
                        .setValue(food.getText().toString());
                myRef.child(name.getText().toString()).child("CLOTH_REQUIRED")
                        .setValue(clothing.getText().toString());
                Toast.makeText(user.this,"DATA SENT",Toast.LENGTH_SHORT).show();

            }
        });


    }
}
